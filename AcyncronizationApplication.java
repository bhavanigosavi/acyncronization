package com.acync.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableJpaRepositories(basePackages = "com.acync.repository")
@EnableSwagger2
@EnableAsync
@EnableScheduling
@SpringBootApplication(scanBasePackages = "com")
public class AcyncronizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcyncronizationApplication.class, args);
	}
}
  