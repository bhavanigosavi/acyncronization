package com.acync.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.acync.module.Employee;
import com.acync.service.Employeeservice;

@RestController
@Component
public class Employeecontroller {
	@Autowired
	private Employeeservice employeeservice;

	@GetMapping(value = "employees")
	public ResponseEntity<List<Employee>> getDetails() {

		List<Employee> emp = employeeservice.findAllemployee();
		return ResponseEntity.status(200).body(emp);
	}
	@PostMapping(value="employees")
	public ResponseEntity<Employee> saveDetails(@Valid @RequestBody Employee employee)
	{		
		return ResponseEntity.status(201).body(employeeservice.saveAllemployee(employee));
	}
	@PutMapping(value="employees/{id}")
	public ResponseEntity<Employee> updatedetails(@RequestBody Employee employee,@PathVariable("id") int id)
	{
		employee.setId(id);
		return ResponseEntity.status(200).body(employeeservice.updateemployee(employee, id));
	}

}
