package com.acync.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.acync.module.Employee;

@Repository
public interface Employeerepository extends JpaRepository<Employee, Integer> {

}
