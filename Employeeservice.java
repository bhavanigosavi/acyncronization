package com.acync.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.acync.module.Employee;
import com.acync.repository.Employeerepository;

@Service
public class Employeeservice {
	@Autowired
	private Employeerepository employeerepository;

	@Async
	public Employee saveAllemployee(Employee employee) {
		System.out.println("save all the employees");
		Employee emp = employeerepository.save(employee);
		try {
			Thread.sleep(5 * 1000);
		} catch (Exception e) {

		}
		System.out.println("saved all the employees");
		return emp;
	}

	@Async
	@Scheduled(cron = "   ?  *")
	public List<Employee> findAllemployee() {
		System.out.println("display all employees time:" + Calendar.getInstance().getTime());
		List<Employee> emp = employeerepository.findAll();
		try {
			Thread.sleep(5 * 1000);
		} catch (Exception e) {

		}
		System.out.println("displayed");
		return emp;
	}

	@Async
	public Employee updateemployee(Employee employee, int id) {
		System.out.println("updating the employee");
		Employee emp = employeerepository.saveAndFlush(employee);
		try {
			Thread.sleep(5 * 1000);
		} catch (Exception e) {

		}
		System.out.println("updated");
		return emp;
	}
}
